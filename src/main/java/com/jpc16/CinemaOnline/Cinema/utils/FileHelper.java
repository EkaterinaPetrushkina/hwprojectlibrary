package com.jpc16.CinemaOnline.Cinema.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Objects;
import java.util.UUID;

import static com.jpc16.CinemaOnline.Cinema.constants.FileDirectoriesConstants.FILMS_UPLOAD_DIRECTORY;

@Slf4j
public class FileHelper {

    private FileHelper(){}

    public static String createFile(final MultipartFile multipartFile) {
        String fileName = StringUtils.cleanPath(Objects.requireNonNull(multipartFile.getOriginalFilename()));
        String resultFileName = "";
        try {
            String uuidName = UUID.randomUUID().toString();
//            Path path = Paths.get(FILMS_UPLOAD_DIRECTORY + "/" + fileName).toAbsolutePath().normalize();
            Path path = Paths.get("images/" + uuidName + "." + fileName).toAbsolutePath().normalize();
            if (!path.toFile().exists()) {
                Files.createDirectories(path);
            }
            Files.copy(multipartFile.getInputStream(), path, StandardCopyOption.REPLACE_EXISTING);
            resultFileName = path.toString();
        } catch (IOException ex) {
            log.error("FileHelper#createFile(): {}", ex.getMessage());
        }
        return resultFileName;
    }
}