package com.jpc16.CinemaOnline.Cinema.mapper;

import com.jpc16.CinemaOnline.Cinema.dto.UserDTO;
import com.jpc16.CinemaOnline.Cinema.model.GenericModel;
import com.jpc16.CinemaOnline.Cinema.model.User;
import com.jpc16.CinemaOnline.Cinema.repository.FavoriteRepository;
import com.jpc16.CinemaOnline.Cinema.repository.RatingRepository;
import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class UserMapper extends GenericMapper<User, UserDTO> {

    private final FavoriteRepository favoriteRepository;
    private final RatingRepository ratingRepository;

    public UserMapper(ModelMapper modelMapper, FavoriteRepository favoriteRepository,
                      RatingRepository ratingRepository) {
        super(User.class, UserDTO.class, modelMapper);
        this.favoriteRepository = favoriteRepository;
        this.ratingRepository = ratingRepository;
    }

    @PostConstruct
    protected void setupMapper() {
        modelMapper.createTypeMap(User.class, UserDTO.class)
                .addMappings(mapping -> mapping.skip(UserDTO::setOrderId))
                .addMappings(mapping -> mapping.skip(UserDTO::setRatingsId))
                .setPostConverter(toDTOConverter());
        modelMapper.createTypeMap(UserDTO.class, User.class)
                .addMappings(mapping -> mapping.skip(User::setFavorites))
                .addMappings(mapping -> mapping.skip(User::setRatings))
                .setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(UserDTO source, User destination) {
        if (!Objects.isNull(source.getOrderId())) {
            destination.setFavorites(favoriteRepository.findAllById(source.getOrderId()));
        } else {
            destination.setFavorites(Collections.emptyList());
        }

        if(!Objects.isNull(source.getRatingsId())) {
            destination.setRatings(ratingRepository.findAllById(source.getRatingsId()));
        } else {
            destination.setRatings(Collections.emptyList());
        }
    }

    @Override
    protected void mapSpecificFields(User source, UserDTO destination) {
        destination.setOrderId(getIds(source));
        destination.setRatingsId(getIds2(source));
    }

    @Override
    protected List<Long> getIds(User entity) {
        return Objects.isNull(entity) || Objects.isNull(entity.getFavorites())
                ? Collections.emptyList()
                : entity.getFavorites().stream()
                .map(GenericModel::getId)
                .collect(Collectors.toList());
    }

    protected List<Long> getIds2(User entity) {
        return Objects.isNull(entity) || Objects.isNull(entity.getRatings())
                ? Collections.emptyList()
                : entity.getRatings().stream()
                .map(GenericModel::getId)
                .collect(Collectors.toList());
    }

}
