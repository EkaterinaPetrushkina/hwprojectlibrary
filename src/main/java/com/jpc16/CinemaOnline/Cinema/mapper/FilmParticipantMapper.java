package com.jpc16.CinemaOnline.Cinema.mapper;

import com.jpc16.CinemaOnline.Cinema.dto.FilmParticipantDTO;
import com.jpc16.CinemaOnline.Cinema.model.FilmParticipant;
import com.jpc16.CinemaOnline.Cinema.model.GenericModel;
import com.jpc16.CinemaOnline.Cinema.repository.FilmRepository;
import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class FilmParticipantMapper extends GenericMapper<FilmParticipant, FilmParticipantDTO> {

    private final FilmRepository filmRepository;

    public FilmParticipantMapper(ModelMapper modelMapper, FilmRepository filmRepository) {
        super(FilmParticipant.class, FilmParticipantDTO.class, modelMapper);
        this.filmRepository = filmRepository;
    }

    @PostConstruct
    protected void setupMapper() {
        modelMapper.createTypeMap(FilmParticipant.class, FilmParticipantDTO.class)
                .addMappings(m -> m.skip(FilmParticipantDTO::setFilmsId))
                .setPostConverter(toDTOConverter());
        modelMapper.createTypeMap(FilmParticipantDTO.class, FilmParticipant.class)
                .addMappings(m -> m.skip(FilmParticipant::setFilms))
                .setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(FilmParticipantDTO source, FilmParticipant destination) {
        if (!Objects.isNull(source.getFilmsId())) {
            destination.setFilms(filmRepository.findAllById(source.getFilmsId()));
        } else {
            destination.setFilms(Collections.emptyList());
        }
    }

    @Override
    protected void mapSpecificFields(FilmParticipant source, FilmParticipantDTO destination) {
        destination.setFilmsId(getIds(source));
    }

    @Override
    protected List<Long> getIds(FilmParticipant entity) {
        return Objects.isNull(entity) || Objects.isNull(entity.getFilms())
                ? Collections.emptyList()
                : entity.getFilms().stream()
                .map(GenericModel::getId)
                .collect(Collectors.toList());
    }
}
