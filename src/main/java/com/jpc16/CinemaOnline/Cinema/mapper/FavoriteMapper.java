package com.jpc16.CinemaOnline.Cinema.mapper;

import com.jpc16.CinemaOnline.Cinema.dto.FavoriteDTO;
import com.jpc16.CinemaOnline.Cinema.model.Favorite;
import com.jpc16.CinemaOnline.Cinema.model.GenericModel;
import com.jpc16.CinemaOnline.Cinema.repository.FilmRepository;
import com.jpc16.CinemaOnline.Cinema.repository.UserRepository;
import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class FavoriteMapper extends GenericMapper<Favorite, FavoriteDTO> {

    private final UserRepository userRepository;
    private final FilmRepository filmRepository;

    public FavoriteMapper(ModelMapper modelMapper, UserRepository userRepository,
                          FilmRepository filmRepository) {
        super(Favorite.class, FavoriteDTO.class, modelMapper);
        this.userRepository = userRepository;
        this.filmRepository = filmRepository;
    }

    @PostConstruct
    protected void setupMapper() {

        modelMapper.createTypeMap(Favorite.class, FavoriteDTO.class)
                .addMappings(m -> m.skip(FavoriteDTO::setUserId))
                .addMappings(m -> m.skip(FavoriteDTO::setFilmId))
                .setPostConverter(toDTOConverter());

        modelMapper.createTypeMap(FavoriteDTO.class, Favorite.class)
                .addMappings(m -> m.skip(Favorite::setUsers))
                .addMappings(m -> m.skip(Favorite::setFilms))
                .setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(FavoriteDTO source, Favorite destination) {

        if (!Objects.isNull(source.getUserId())) {
            destination.setUsers(userRepository.findAllById(source.getUserId()));
        } else {
            destination.setUsers(Collections.emptyList());
        }

        if (!Objects.isNull(source.getFilmId())) {
            destination.setFilms(filmRepository.findAllById(source.getFilmId()));
        } else {
            destination.setFilms(Collections.emptyList());
        }

    }

    @Override
    protected void mapSpecificFields(Favorite source, FavoriteDTO destination) {
        destination.setUserId(getIds(source));
        destination.setFilmId(getIds2(source));
    }

    @Override
    protected List<Long> getIds(Favorite entity) {
        return Objects.isNull(entity) || Objects.isNull(entity.getUsers())
                ? Collections.emptyList()
                : entity.getUsers().stream().map(GenericModel::getId)
                .collect(Collectors.toList());
    }

    protected List<Long> getIds2(Favorite entity) {
        return Objects.isNull(entity) || Objects.isNull(entity.getFilms())
                ? Collections.emptyList()
                : entity.getFilms().stream().map(GenericModel::getId)
                .collect(Collectors.toList());
    }

}
