package com.jpc16.CinemaOnline.Cinema.mapper;

import com.jpc16.CinemaOnline.Cinema.dto.RatingDTO;
import com.jpc16.CinemaOnline.Cinema.model.Rating;
import com.jpc16.CinemaOnline.Cinema.repository.FilmRepository;
import com.jpc16.CinemaOnline.Cinema.repository.UserRepository;
import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class RatingMapper extends GenericMapper<Rating, RatingDTO> {

    private final FilmRepository filmRepository;
    private final UserRepository userRepository;

    public RatingMapper(ModelMapper modelMapper, FilmRepository filmRepository,
                        UserRepository userRepository) {
        super(Rating.class, RatingDTO.class, modelMapper);
        this.filmRepository = filmRepository;
        this.userRepository = userRepository;
    }

    @PostConstruct
    protected void setupMapper() {

        modelMapper.createTypeMap(Rating.class, RatingDTO.class)
                .addMappings(mapping -> mapping.skip(RatingDTO::setFilmId))
                .addMappings(mapping -> mapping.skip(RatingDTO::setUserId))
                .setPostConverter(toDTOConverter());

        modelMapper.createTypeMap(RatingDTO.class, Rating.class)
                .addMappings(mapping -> mapping.skip(Rating::setFilmId))
                .addMappings(mapping -> mapping.skip(Rating::setUserId))
                .setPostConverter(toEntityConverter());

    }

    @Override
    protected void mapSpecificFields(RatingDTO source, Rating destination) {

        destination.setFilmId(filmRepository.findById(source.getFilmId())
                .orElseThrow(() -> new RuntimeException("Film not found")));

        destination.setUserId(userRepository.findById(source.getUserId())
                .orElseThrow(() -> new RuntimeException("User not found")));

    }

    @Override
    protected void mapSpecificFields(Rating source, RatingDTO destination) {

        destination.setFilmId(source.getFilmId().getId());
        destination.setUserId(source.getUserId().getId());
    }

    @Override
    protected List<Long> getIds(Rating entity) {
        throw new UnsupportedOperationException("Method not available.");
    }
}
