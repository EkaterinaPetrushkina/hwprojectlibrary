package com.jpc16.CinemaOnline.Cinema.mapper;

import com.jpc16.CinemaOnline.Cinema.dto.FilmDTO;
import com.jpc16.CinemaOnline.Cinema.model.Film;
import com.jpc16.CinemaOnline.Cinema.model.GenericModel;
import com.jpc16.CinemaOnline.Cinema.repository.FilmParticipantRepository;
import com.jpc16.CinemaOnline.Cinema.repository.FavoriteRepository;
import com.jpc16.CinemaOnline.Cinema.repository.RatingRepository;
import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class FilmMapper extends GenericMapper<Film, FilmDTO> {

    private final FilmParticipantRepository filmParticipantRepository;
    private final FavoriteRepository favoriteRepository;
    private final RatingRepository ratingRepository;


    public FilmMapper(ModelMapper modelMapper, FilmParticipantRepository filmParticipantRepository,
                      FavoriteRepository favoriteRepository, RatingRepository ratingRepository) {
        super(Film.class, FilmDTO.class, modelMapper);
        this.filmParticipantRepository = filmParticipantRepository;
        this.favoriteRepository = favoriteRepository;
        this.ratingRepository = ratingRepository;
    }

    @PostConstruct
    protected void setupMapper() {
        modelMapper.createTypeMap(Film.class, FilmDTO.class)
                .addMappings(m -> m.skip(FilmDTO::setFilmParticipantsId))
                .addMappings(m -> m.skip(FilmDTO::setOrdersId))
                .addMappings(m -> m.skip(FilmDTO::setRatingsId))
                .setPostConverter(toDTOConverter());
        modelMapper.createTypeMap(FilmDTO.class, Film.class)
                .addMappings(m -> m.skip(Film::setFilmParticipants))
                .addMappings(m -> m.skip(Film::setFavorites))
                .addMappings(m -> m.skip(Film::setRatings))
                .setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(FilmDTO source, Film destination) {
        if (!Objects.isNull(source.getFilmParticipantsId())) {
            destination.setFilmParticipants(filmParticipantRepository.findAllById(source.getFilmParticipantsId()));
        } else {
            destination.setFilmParticipants(Collections.emptyList());
        }

        if (!Objects.isNull(source.getOrdersId())) {
            destination.setFavorites(favoriteRepository.findAllById(source.getOrdersId()));
        } else {
            destination.setFavorites(Collections.emptyList());
        }

        if (!Objects.isNull(source.getRatingsId())) {
            destination.setRatings(ratingRepository.findAllById(source.getRatingsId()));
        } else {
            destination.setRatings(Collections.emptyList());
        }
    }

    @Override
    protected void mapSpecificFields(Film source, FilmDTO destination) {
        destination.setFilmParticipantsId(getIds(source));
        destination.setOrdersId(getIds2(source));
        destination.setRatingsId(getIds3(source));
    }

    @Override
    protected List<Long> getIds(Film entity) {
        return Objects.isNull(entity) || Objects.isNull(entity.getFilmParticipants())
                ? Collections.emptyList()
                : entity.getFilmParticipants().stream()
                .map(GenericModel::getId)
                .collect(Collectors.toList());
    }

    protected List<Long> getIds2(Film entity) {
        return Objects.isNull(entity) || Objects.isNull(entity.getFavorites())
                ? Collections.emptyList()
                : entity.getFavorites().stream()
                .map(GenericModel::getId)
                .collect(Collectors.toList());
    }

    protected List<Long> getIds3(Film entity) {
        return Objects.isNull(entity) || Objects.isNull(entity.getRatings())
                ? Collections.emptyList()
                : entity.getRatings().stream()
                .map(GenericModel::getId)
                .collect(Collectors.toList());
    }

}
