package com.jpc16.CinemaOnline.Cinema.mapper;

import com.jpc16.CinemaOnline.Cinema.dto.GenericDTO;
import com.jpc16.CinemaOnline.Cinema.model.GenericModel;

import java.util.List;

public interface Mapper<E extends GenericModel, D extends GenericDTO> {

    E toEntity(D dto);
    D toDTO(E entity);

    List<E> toEntities(List<D> dtos);
    List<D> toDTOs(List<E> entities);
}
