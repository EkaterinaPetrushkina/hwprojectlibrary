package com.jpc16.CinemaOnline.Cinema.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
@NoArgsConstructor
public class RatingDTO extends GenericDTO {

    private Integer rate;
    private Long filmId;
    private Long userId;

}
