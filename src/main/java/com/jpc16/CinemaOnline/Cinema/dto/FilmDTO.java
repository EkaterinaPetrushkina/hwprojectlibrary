package com.jpc16.CinemaOnline.Cinema.dto;

import com.jpc16.CinemaOnline.Cinema.model.lists.Category;
import com.jpc16.CinemaOnline.Cinema.model.lists.Genre;
import lombok.*;

import java.time.LocalDate;
import java.util.List;

@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FilmDTO extends GenericDTO {

    private String title;
    private LocalDate premierYear;
    private String country;
    private Genre genre;
    private Category category;
    private String duration;
    private String picture;
    private String onlineCopyPath;
    private List<Long> filmParticipantsId;
    private List<Long> ordersId;
    private List<Long> ratingsId;

}
