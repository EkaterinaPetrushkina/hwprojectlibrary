package com.jpc16.CinemaOnline.Cinema.dto;

import com.jpc16.CinemaOnline.Cinema.model.lists.Status;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@ToString
@Getter
@Setter
@NoArgsConstructor
public class FavoriteDTO extends GenericDTO {

    private Status status;
    private List<Long> userId;
    private List<Long> filmId;

}
