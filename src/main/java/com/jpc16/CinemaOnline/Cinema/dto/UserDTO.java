package com.jpc16.CinemaOnline.Cinema.dto;

import com.jpc16.CinemaOnline.Cinema.model.lists.Gender;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;
import java.util.List;

@ToString
@Getter
@Setter
@NoArgsConstructor
public class UserDTO extends GenericDTO {

    private String login;
    private String password;
    private Gender gender;
    private String name;
    private String surname;
    private String patronymic;
    private Date birthDate;
    private String phone;
    private String address;
    private String email;
    private String changePasswordToken;
    private RoleDTO roleId;
    private List<Long> orderId;
    private List<Long> ratingsId;
}
