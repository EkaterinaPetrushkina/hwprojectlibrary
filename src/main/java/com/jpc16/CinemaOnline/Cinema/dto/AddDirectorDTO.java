package com.jpc16.CinemaOnline.Cinema.dto;

import lombok.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class AddDirectorDTO {

    private Long filmId;
    private Long directorId;
}
