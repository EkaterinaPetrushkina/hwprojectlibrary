package com.jpc16.CinemaOnline.Cinema.dto;

import com.jpc16.CinemaOnline.Cinema.model.lists.FilmRoles;
import com.jpc16.CinemaOnline.Cinema.model.lists.Gender;
import lombok.*;

import java.time.LocalDate;
import java.util.List;

@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FilmParticipantDTO extends GenericDTO {

    private Gender gender;
    private String name;
    private String surname;
    private String patronymic;
    private List<FilmRoles> position;
    private LocalDate birthDate;
    private String birthCountry;
    private List<Long> filmsId;

}
