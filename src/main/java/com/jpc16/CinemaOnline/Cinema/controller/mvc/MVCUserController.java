package com.jpc16.CinemaOnline.Cinema.controller.mvc;

import com.jpc16.CinemaOnline.Cinema.dto.UserDTO;
import com.jpc16.CinemaOnline.Cinema.service.UserService;
import jakarta.websocket.server.PathParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

import static com.jpc16.CinemaOnline.Cinema.constants.UserRolesConstants.ADMIN;
import static com.jpc16.CinemaOnline.Cinema.constants.UserRolesConstants.MODERATOR;

@Slf4j
@Controller
public class MVCUserController {

    private final UserService userService;

    public MVCUserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/registration")
    public String registration(Model model) {
        model.addAttribute("userForm", new UserDTO());
        return "registration";
    }

    @PostMapping("/registration")
    public String registration(@ModelAttribute("userForm") UserDTO userDTO,
                               BindingResult bindingResult) {
        if (userDTO.getLogin().equalsIgnoreCase(ADMIN) || userService.getUserByLogin(userDTO.getLogin()) != null) {
            bindingResult.rejectValue("login", "error.login", "Данный логин уже используется");
            return "registration";
        }
        if (userService.getUserByEmail(userDTO.getEmail()) != null) {
            bindingResult.rejectValue("email", "error.email", "Данная почта уже используется");
            return "registration";
        }
        userService.create(userDTO);
        return "redirect:login";
    }

    @GetMapping("/remember_password")
    public String rememberPassword() {
        return "user/rememberPassword";
    }

    @PostMapping("/remember_password")
    public String rememberPassword(@ModelAttribute("changePasswordForm") UserDTO userDTO) {
        userDTO = userService.getUserByEmail(userDTO.getEmail());
        if (Objects.isNull(userDTO)) {
            return "error";
        } else {
            userService.sendChangePasswordEmail(userDTO);
            return "redirect:/login";
        }
    }

    @GetMapping("/change_password")
    public String changePassword(@PathParam(value = "uuid") String uuid,
                                 Model model) {
        model.addAttribute("uuid", uuid);
        return "user/changePassword";
    }

    @PostMapping("/change_password")
    public String changePassword (@PathParam(value = "uuid") String uuid,
                                  @ModelAttribute("changePasswordForm") UserDTO userDTO) {
        userService.changePassword(uuid, userDTO.getPassword());
        return "redirect:/login";
    }

    @GetMapping("/users")
    public String viewAllUsers(Model model) {
        List<UserDTO> users;
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        if(ADMIN.equalsIgnoreCase(username) || MODERATOR.equalsIgnoreCase(userService.checkRole(username))){
            users = userService.listAll();
            model.addAttribute("users", users);
            return "user/viewAllUsers";
        } else {
            return "error";
        }
    }

    @GetMapping("/profile")
    public String profile(Model model) {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        model.addAttribute("user", userService.getUserByLogin(username));
        return "redirect:/user/profile";
    }

    @GetMapping("/profile/change")
    public String changeProfile(Model model) {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        model.addAttribute("userForm", userService.getUserByLogin(username));
        return "user/changeUserProfile";
    }

    @PostMapping("/profile/change")
    public String changeProfile(@ModelAttribute("changeProfileForm") UserDTO userDTO) {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        userService.update(userService.getUserByLogin(username));
        return "redirect:/profile";
    }


}
