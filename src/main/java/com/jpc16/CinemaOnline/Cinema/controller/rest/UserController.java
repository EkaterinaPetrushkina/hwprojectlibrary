//package com.jpc16.hwlibraryproject.Cinema.controller.rest;
//
//import com.jpc16.hwlibraryproject.Cinema.config.jwt.JWTTokenUtil;
//import com.jpc16.hwlibraryproject.Cinema.dto.LoginDTO;
//import com.jpc16.hwlibraryproject.Cinema.dto.UserDTO;
//import com.jpc16.hwlibraryproject.Cinema.model.User;
//import com.jpc16.hwlibraryproject.Cinema.service.GenericService;
//import com.jpc16.hwlibraryproject.Cinema.service.UserService;
//import com.jpc16.hwlibraryproject.Cinema.service.userdetails.CustomUserDetailService;
//import io.swagger.v3.oas.annotations.Operation;
//import io.swagger.v3.oas.annotations.security.SecurityRequirement;
//import io.swagger.v3.oas.annotations.tags.Tag;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.MediaType;
//import org.springframework.http.ResponseEntity;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.web.bind.annotation.*;
//
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//@Slf4j
//@RestController
//@RequestMapping(name = "/users")
//@SecurityRequirement(name = "Bearer Authentication")
//@CrossOrigin(origins = "*", allowedHeaders = "*")
//@Tag(name = "User", description = "Controllers for work with users.")
//public class UserController extends GenericController<User, UserDTO> {
//
//    private final CustomUserDetailService customUserDetailService;
//    private final JWTTokenUtil jwtTokenUtil;
//    private final UserService userService;
//
//    public UserController(GenericService<User, UserDTO> genericService,CustomUserDetailService customUserDetailService,
//                          JWTTokenUtil jwtTokenUtil, UserService userService) {
//        super(genericService);
//        this.customUserDetailService = customUserDetailService;
//        this.jwtTokenUtil = jwtTokenUtil;
//        this.userService = userService;
//    }
//
//    @Operation(method = "getAllOrders", description = "Get list of all orders")
//    @RequestMapping(value = "/getAllOrders", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<List<Long>> getAllOrders(@RequestParam(value = "user_id") Long userId) {
//        return ResponseEntity.status(HttpStatus.OK).body(((UserService) service).getAllOrders(userId));
//    }
//
//    @PostMapping("/auth")
//    public ResponseEntity<?> auth(@RequestBody LoginDTO loginDTO) {
//        Map<String, Object> response = new HashMap<>();
//        UserDetails foundUser = customUserDetailService.loadUserByUsername(loginDTO.getLogin());
//        log.info("foundUser: {}", foundUser);
//        if (userService.checkPassword(loginDTO.getPassword(), foundUser)) {
//            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Authorization error. \n Wrong password");
//        }
//        String token = jwtTokenUtil.generateToken(foundUser);
//        response.put("token", token);
//        response.put("username", foundUser.getAuthorities());
//        return ResponseEntity.ok().body(response);
//    }
//}
