package com.jpc16.CinemaOnline.Cinema.controller.mvc;

import com.jpc16.CinemaOnline.Cinema.dto.FavoriteDTO;
import com.jpc16.CinemaOnline.Cinema.service.FilmService;
import com.jpc16.CinemaOnline.Cinema.service.FavoriteService;
import com.jpc16.CinemaOnline.Cinema.service.userdetails.CustomUserDetails;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@Slf4j
@RequestMapping("/favorite")
public class MVCFavoriteController {

    private final FavoriteService favoriteService;
    private final FilmService filmService;

    public MVCFavoriteController(FavoriteService favoriteService, FilmService filmService) {
        this.favoriteService = favoriteService;
        this.filmService = filmService;
    }

    @GetMapping
    public String userFavorite(@RequestParam(value = "page", defaultValue = "1") int page,
                               @RequestParam(value = "size", defaultValue = "5") int pageSize,
                               Model model) {
        PageRequest pageRequest = PageRequest.of(page - 1, pageSize);
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        Page<FavoriteDTO> favoriteDTOPage = favoriteService.listAllFavorite(username, pageRequest);
        model.addAttribute("favoriteFilms", favoriteDTOPage);
        return "order/viewAllFavorite";
    }

}
