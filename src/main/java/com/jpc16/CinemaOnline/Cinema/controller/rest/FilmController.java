package com.jpc16.CinemaOnline.Cinema.controller.rest;

import com.jpc16.CinemaOnline.Cinema.dto.FilmDTO;
import com.jpc16.CinemaOnline.Cinema.model.Film;
import com.jpc16.CinemaOnline.Cinema.service.FilmService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(name = "/films")
@Tag(name = "Films", description = "Controllers for work with films.")
public class FilmController extends GenericController<Film, FilmDTO> {

    public FilmController(FilmService service) {
        super(service);
    }


//    @Operation(description = "Add director to film")
//    @RequestMapping(value = "/addDirector", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<FilmDTO> addDirector(@RequestParam(value = "filmId") Long filmId,
//                                            @RequestParam(value = "directorId") Long directorId) {
//       return ResponseEntity.status(HttpStatus.OK).body(((FilmService)service).addDirector(filmId, directorId));
//    }


}
