package com.jpc16.CinemaOnline.Cinema.controller.rest;

import com.jpc16.CinemaOnline.Cinema.dto.GenericDTO;
import com.jpc16.CinemaOnline.Cinema.model.GenericModel;
import com.jpc16.CinemaOnline.Cinema.service.GenericService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Slf4j
public abstract class GenericController<E extends GenericModel,D extends GenericDTO> {

    protected GenericService<E, D> service;

    public GenericController(GenericService<E, D> service) {
        this.service = service;
    }

    @Operation(description = "Get object by id", method = "getById()")
    @RequestMapping(value = "/getById", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<D> getById(@RequestParam(value = "id") Long id) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(service.getOne(id));
    }

    @Operation(description = "Get all objects", method = "getAll()")
    @RequestMapping(value = "/getAll", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<D>> getAll() {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(service.listAll());
    }

    @Operation(description = "Create object", method = "create()")
    @RequestMapping(value = "/create", method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<D> create(@RequestBody D newEntity) {
        log.info(newEntity.toString());
        return ResponseEntity.status(HttpStatus.CREATED).body(service.create(newEntity));
    }

    @Operation(description = "Update object", method = "update()")
    @RequestMapping(value = "/update", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<D> update(@RequestBody D updateEntity, @RequestParam(value = "id") Long id) {
        updateEntity.setId(id);
        log.info(updateEntity.toString());
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(service.update(updateEntity));
    }

    @Operation(description = "Delete object", method = "update()")
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public void delete(@PathVariable(value = "id") Long id) {
        service.delete(id);
    }
}
