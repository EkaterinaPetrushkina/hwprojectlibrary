package com.jpc16.CinemaOnline.Cinema.controller.rest;


import com.jpc16.CinemaOnline.Cinema.dto.FavoriteDTO;
import com.jpc16.CinemaOnline.Cinema.model.Favorite;
import com.jpc16.CinemaOnline.Cinema.service.FavoriteService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/orders")
@Tag(name = "Orders", description = "Controllers for work with orders.")
public class OrderController extends GenericController<Favorite, FavoriteDTO> {

    public OrderController(FavoriteService favoriteService) {
        super(favoriteService);
    }

//    @Operation(method = "rentFilm", description = "Rent film/")
//    @RequestMapping(value = "/rentFilm", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<FavoriteDTO> rentFilm(@RequestParam(value = "user_id") Long userId,
//                                                      @RequestParam(value = "film_id") Long filmId) {
//        return ResponseEntity.status(HttpStatus.CREATED).body(((FavoriteService) service).orderFilm(userId, filmId));
//    }
}
