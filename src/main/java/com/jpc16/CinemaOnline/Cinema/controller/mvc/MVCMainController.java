package com.jpc16.CinemaOnline.Cinema.controller.mvc;

import com.jpc16.CinemaOnline.Cinema.service.FilmService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class MVCMainController {

    private final FilmService filmService;

    public MVCMainController(FilmService filmService) {
        this.filmService = filmService;
    }

    @GetMapping
    public String index(Model model) {
        model.addAttribute("popular", filmService.getPopularFilms());
        return "index";
    }

    @GetMapping("/about")
    public String about() {
        return "about";
    }


}
