package com.jpc16.CinemaOnline.Cinema.controller.mvc;

import com.jpc16.CinemaOnline.Cinema.dto.FilmParticipantDTO;
import com.jpc16.CinemaOnline.Cinema.dto.FilmDTO;
import com.jpc16.CinemaOnline.Cinema.exception.MyDeleteException;
import com.jpc16.CinemaOnline.Cinema.service.FilmParticipantService;
import com.jpc16.CinemaOnline.Cinema.service.FilmService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static com.jpc16.CinemaOnline.Cinema.constants.UserRolesConstants.ADMIN;

@Slf4j
@Controller
@RequestMapping("/films")
public class MVCFilmController {

    private final FilmService filmService;
    private final FilmParticipantService filmParticipantService;

    public MVCFilmController(FilmService filmService, FilmParticipantService filmParticipantService) {
        this.filmService = filmService;
        this.filmParticipantService = filmParticipantService;
    }

    @GetMapping
    public String getAllFilms(@RequestParam(value = "page", defaultValue = "1") int page,
                              @RequestParam(value = "size", defaultValue = "5") int pageSize,
                              Model model) {
        PageRequest pageRequest = PageRequest.of(page - 1, pageSize, Sort.by(Sort.Direction.ASC, "title"));
        Page<FilmDTO> films;
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        if(ADMIN.equalsIgnoreCase(username)) {
            films = filmService.getAllFilms(pageRequest);
        } else {
            films = filmService.listAllNotDeleted(pageRequest);
        }
        model.addAttribute("films", films);
        return "films/viewAllFilmss";
    }

    @PostMapping("/search")
    public String search(@RequestParam(value = "page", defaultValue = "1") int page,
                         @RequestParam(value = "size", defaultValue = "5") int pageSize,
                         @ModelAttribute("filmSearchForm") FilmDTO filmDTO,
                         Model model) {
        PageRequest pageRequest = PageRequest.of(page - 1, pageSize, Sort.by(Sort.Direction.ASC, "title"));
        model.addAttribute("film", filmService.searchFilm(filmDTO, pageRequest));
        return "films/viewAllFilmss";
    }

    @GetMapping("/add")
    public String create(Model model) {
        model.addAttribute("directors", filmParticipantService.listAllNotDeleted());
        return "films/addFilm";
    }

    @PostMapping("/add")
    public String create(@ModelAttribute("filmForm") FilmDTO film,
                         @RequestParam("video") MultipartFile videoFile,
                         @RequestParam("picture") MultipartFile pictureFile) {
        log.info(film.toString());
        if (videoFile != null && videoFile.getSize() > 0 && pictureFile != null && pictureFile.getSize() > 0) {
            filmService.create(film, videoFile, pictureFile);
        } else {
            filmService.create(film);
        }
        return "redirect:/films";
    }

    @GetMapping("/{id}")
    public String getOne(@PathVariable Long id,
                         Model model) {
        model.addAttribute("film", filmService.getOne(id));
        return "films/viewFilm";
    }

    @GetMapping("/{id}/update")
    public String update(@PathVariable Long id,
                         Model model) {
        model.addAttribute("film", filmService.getOne(id));
        return "films/updateFilm";
    }

    @PostMapping("/{id}/update")
    public String update(@ModelAttribute("filmForm") FilmDTO filmDTO) {
        filmService.update(filmDTO);
        return "redirect:/films";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable Long id) throws MyDeleteException {
        filmService.delete(id);
        return "redirect:/films";
    }

    @GetMapping(value = "/download", produces = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ResponseBody
    public ResponseEntity<Resource> downloadFilm(@Param(value = "filmId") Long filmId) throws IOException {
        FilmDTO filmDTO = filmService.getOne(filmId);
        Path path = Paths.get(filmDTO.getOnlineCopyPath());
        ByteArrayResource resource = new ByteArrayResource(Files.readAllBytes(path));
        return ResponseEntity.ok()
                .headers(createHeaders(path.getFileName().toString()))
                .contentLength(path.toFile().length())
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .body(resource);
    }

    private HttpHeaders createHeaders(final String name) {
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename" + name);
        headers.add("Cache-Control", "no-cache, no-store");
        headers.add("Expires", "0");
        return headers;
    }

}
