package com.jpc16.CinemaOnline.Cinema.controller.mvc;

import com.jpc16.CinemaOnline.Cinema.dto.FilmParticipantDTO;
import com.jpc16.CinemaOnline.Cinema.service.FilmParticipantService;
import com.jpc16.CinemaOnline.Cinema.service.FilmService;
import com.jpc16.CinemaOnline.Cinema.service.RatingService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import static com.jpc16.CinemaOnline.Cinema.constants.UserRolesConstants.ADMIN;

@Slf4j
@Controller
@RequestMapping("/participants")
public class MVCFilmParticipantController {
    private final FilmParticipantService filmParticipantService;
    private final FilmService filmService;
    private final RatingService ratingService;

    public MVCFilmParticipantController(FilmParticipantService filmParticipantService, FilmService filmService,
                                        RatingService ratingService) {
        this.filmParticipantService = filmParticipantService;
        this.filmService = filmService;
        this.ratingService = ratingService;
    }

    @GetMapping
    public String getAllParticipants(@RequestParam(value = "page", defaultValue = "1") int page,
                                     @RequestParam(value = "size", defaultValue = "5") int pageSize,
                                     Model model) {
        PageRequest pageRequest = PageRequest.of(page - 1, pageSize, Sort.by(Sort.Direction.ASC, "surname"));
        Page<FilmParticipantDTO> result;

        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        if (ADMIN.equalsIgnoreCase(username)) {
            result = filmParticipantService.listAll(pageRequest);
        } else {
            result = filmParticipantService.listAllNotDeleted(pageRequest);
        }

        model.addAttribute("participants", result);
        return "participants/viewAllParticipants";
    }

    @PostMapping
    public String searchParticipant(@RequestParam(value = "page", defaultValue = "1") int page,
                                    @RequestParam(value = "size", defaultValue = "5") int pageSize,
                                    @ModelAttribute("searchForm") FilmParticipantDTO filmParticipantDTO,
                                    Model model) {
        PageRequest pageRequest = PageRequest.of(page, pageSize, Sort.by(Sort.Direction.ASC, "surname"));
        model.addAttribute("participant", filmParticipantService.searchParticipants(filmParticipantDTO, pageRequest));
        return "participants/viewAllParticipants";
    }

    @GetMapping("/{id}")
    public String getOne(@PathVariable Long id, Model model) {
        model.addAttribute("participant", filmParticipantService.getOne(id));
        model.addAttribute("films", filmService.findByParticipantId(id));
        return "participants/viewParticipant";
    }

    @GetMapping("/add")
    public String create(Model model) {
        model.addAttribute("films", filmService.listAllNotDeleted());
        return "participants/addParticipant";
    }

    @PostMapping("/add")
    public String create(@ModelAttribute("participantForm") FilmParticipantDTO participantDTO) {
        log.info(participantDTO.toString());
        filmParticipantService.create(participantDTO);
        return "redirect:/participants";
    }


    @GetMapping("/{id}/update")
    public String update(@PathVariable Long id, Model model) {
        model.addAttribute("participant", filmParticipantService.getOne(id));
        return "participants/updateParticipant";
    }

    @PostMapping("/{id}/update")
    public String update(@ModelAttribute("participantForm") FilmParticipantDTO participantDTO) {
        filmParticipantService.update(participantDTO);
        return "participants/updateParticipant";
    }


}
