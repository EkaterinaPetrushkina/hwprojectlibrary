package com.jpc16.CinemaOnline.Cinema.controller.rest;

import com.jpc16.CinemaOnline.Cinema.dto.FilmParticipantDTO;
import com.jpc16.CinemaOnline.Cinema.model.FilmParticipant;
import com.jpc16.CinemaOnline.Cinema.service.FilmParticipantService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/directors")
@SecurityRequirement(name = "Bearer Authentication")
@CrossOrigin(origins = "*", allowedHeaders = "*")
@Tag(name = "Directors", description = "Controllers for work with directors of the films.")
public class FilmParticipantController extends GenericController<FilmParticipant, FilmParticipantDTO> {

    public FilmParticipantController(FilmParticipantService service) {
        super(service);
    }

    @Operation(description = "Add film to director")
    @RequestMapping(value = "/addFilm", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public String add(@ModelAttribute("directorForm") FilmParticipantDTO newDirector) {
        log.info(newDirector.toString());
        service.create(newDirector);
        return "redirect:/directors";
    }



}
