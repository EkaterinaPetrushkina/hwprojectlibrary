package com.jpc16.CinemaOnline.Cinema.controller.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/login")
public class MVCLoginController {

    @GetMapping
    public String login() {
        return "login";
    }
}
