package com.jpc16.CinemaOnline.Cinema.service;

import com.jpc16.CinemaOnline.Cinema.dto.FilmParticipantDTO;
import com.jpc16.CinemaOnline.Cinema.mapper.FilmParticipantMapper;
import com.jpc16.CinemaOnline.Cinema.model.FilmParticipant;
import com.jpc16.CinemaOnline.Cinema.repository.FilmParticipantRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class FilmParticipantService extends GenericService<FilmParticipant, FilmParticipantDTO> {


    public FilmParticipantService(FilmParticipantRepository repository,
                                  FilmParticipantMapper mapper) {
        super(repository, mapper);
    }

//    public FilmParticipantDTO addFilm(Long directorId, Long filmId) {
//        Film film = filmRepository.findById(filmId)
//                .orElseThrow(() -> new EntityNotFoundException("Information by this id not found."));
//        FilmParticipantDTO directorDTO = getOne(directorId);
//        directorDTO.getFilmsId().add(film.getId());
//        update(directorDTO);
//        return directorDTO;
//    }

//    public FilmParticipantDTO addFilm(AddDirectorDTO addDirectorDTO) {
//        FilmParticipantDTO director = getOne(addDirectorDTO.getDirectorId());
//        director.getFilmsId().add(addDirectorDTO.getFilmId());
//        update(director);
//        return director;
//    }


    @Override
    public FilmParticipantDTO update(FilmParticipantDTO participantDTO) {
        FilmParticipantDTO filmParticipantDTO = getOne(participantDTO.getId());
        if (!Objects.isNull(participantDTO.getGender())) {
            filmParticipantDTO.setGender(participantDTO.getGender());
        }
        if (!Objects.isNull(participantDTO.getSurname())) {
            filmParticipantDTO.setSurname(participantDTO.getSurname());
        }
        if (!Objects.isNull(participantDTO.getName())) {
            filmParticipantDTO.setName(participantDTO.getName());
        }
        if (!Objects.isNull(participantDTO.getPatronymic())) {
            filmParticipantDTO.setPatronymic(participantDTO.getPatronymic());
        }
        if (!Objects.isNull(participantDTO.getPosition())) {
            filmParticipantDTO.setPosition(participantDTO.getPosition());
        }
        if (!Objects.isNull(participantDTO.getBirthDate())) {
            filmParticipantDTO.setBirthDate(participantDTO.getBirthDate());
        }
        if (!Objects.isNull(participantDTO.getBirthCountry())) {
            filmParticipantDTO.setBirthCountry(participantDTO.getBirthCountry());
        }
        return super.update(filmParticipantDTO);
    }

    public Object searchParticipants(FilmParticipantDTO filmParticipantDTO, Pageable pageable) {

        Page<FilmParticipant> filmParticipants = ((FilmParticipantRepository) repository)
                .findParticipants(filmParticipantDTO.getGender(),
                        filmParticipantDTO.getSurname(),
                        filmParticipantDTO.getName(),
                        filmParticipantDTO.getPatronymic(),
                        filmParticipantDTO.getPosition(),
                        filmParticipantDTO.getBirthDate(),
                        filmParticipantDTO.getBirthCountry(),
                        pageable);

        List<FilmParticipantDTO> result = mapper.toDTOs(filmParticipants.getContent());
        return new PageImpl<>(result, pageable, filmParticipants.getTotalElements());
    }
}
