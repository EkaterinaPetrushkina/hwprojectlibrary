package com.jpc16.CinemaOnline.Cinema.service;

import com.jpc16.CinemaOnline.Cinema.dto.RatingDTO;
import com.jpc16.CinemaOnline.Cinema.mapper.RatingMapper;
import com.jpc16.CinemaOnline.Cinema.model.Rating;
import com.jpc16.CinemaOnline.Cinema.repository.RatingRepository;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;
import java.util.List;

@Service
public class RatingService extends GenericService<Rating, RatingDTO> {

    public RatingService(RatingRepository repository, RatingMapper mapper) {
        super(repository, mapper);
    }


    public String findByFilmId(Long id) {
        List<Rating> ratings = ((RatingRepository) repository).findByFilmId(id);
        double averageRating = ratings.stream().mapToInt(Rating::getRate).average().orElse(0);
        DecimalFormat decimalFormat = new DecimalFormat("#.#");
        String result = decimalFormat.format(averageRating);
        return result;
    }

}
