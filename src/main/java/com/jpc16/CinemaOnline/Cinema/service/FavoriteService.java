package com.jpc16.CinemaOnline.Cinema.service;

import com.jpc16.CinemaOnline.Cinema.dto.FavoriteDTO;
import com.jpc16.CinemaOnline.Cinema.mapper.FavoriteMapper;
import com.jpc16.CinemaOnline.Cinema.model.Favorite;
import com.jpc16.CinemaOnline.Cinema.model.User;
import com.jpc16.CinemaOnline.Cinema.repository.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FavoriteService extends GenericService<Favorite, FavoriteDTO> {

    private final FilmService filmService;
    private final UserRepository userRepository;

    public FavoriteService(FavoriteRepository favoriteRepository, FavoriteMapper favoriteMapper, FilmService filmService,
                           UserRepository userRepository) {
        super(favoriteRepository, favoriteMapper);
        this.filmService = filmService;
        this.userRepository = userRepository;
    }

    public Page<FavoriteDTO> listAllFavorite(String username, Pageable pageRequest) {
        User user = userRepository.findUserByLogin(username);
        Page<Favorite> favorites = ((FavoriteRepository) repository).findAllByUser(user.getId(), pageRequest);
        List<FavoriteDTO> favoriteDTOS = mapper.toDTOs(favorites.getContent());
        return new PageImpl<>(favoriteDTOS, pageRequest, favorites.getTotalElements());

    }


}
