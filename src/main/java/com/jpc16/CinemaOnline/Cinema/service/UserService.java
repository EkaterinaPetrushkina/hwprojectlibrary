package com.jpc16.CinemaOnline.Cinema.service;

import com.jpc16.CinemaOnline.Cinema.constants.MailConstants;
import com.jpc16.CinemaOnline.Cinema.dto.RoleDTO;
import com.jpc16.CinemaOnline.Cinema.dto.UserDTO;
import com.jpc16.CinemaOnline.Cinema.mapper.UserMapper;
import com.jpc16.CinemaOnline.Cinema.model.GenericModel;
import com.jpc16.CinemaOnline.Cinema.model.User;
import com.jpc16.CinemaOnline.Cinema.repository.UserRepository;
import com.jpc16.CinemaOnline.Cinema.utils.MailUtils;
import jakarta.persistence.EntityNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

@Slf4j
@Service
public class UserService extends GenericService<User, UserDTO> {

    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final JavaMailSender javaMailSender;
    public UserService(UserRepository userRepository, UserMapper userMapper,
                       BCryptPasswordEncoder bCryptPasswordEncoder, JavaMailSender javaMailSender) {
        super(userRepository, userMapper);
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;

        this.javaMailSender = javaMailSender;
    }

    @Override
    public UserDTO create(UserDTO newObject) {
        RoleDTO roleDTO = new RoleDTO();
        roleDTO.setId(1L);
        newObject.setRoleId(roleDTO);
        newObject.setPassword(bCryptPasswordEncoder.encode(newObject.getPassword()));
        newObject.setCreatedWhen(LocalDateTime.now());
        return mapper.toDTO(repository.save(mapper.toEntity(newObject)));
    }

    public List<Long> getAllOrders(Long userId) {
        User user = repository.findById(userId)
                .orElseThrow(() -> new EntityNotFoundException("User not found."));
        return Objects.isNull(user) || Objects.isNull(user.getFavorites())
                ? Collections.emptyList()
                : user.getFavorites().stream()
                .map(GenericModel::getId)
                .collect(Collectors.toList());
    }

    public UserDTO getUserByLogin(final String login) {
        return mapper.toDTO(((UserRepository) repository).findUserByLogin(login));
    }

    public UserDTO getUserByEmail(final String email) {
        return mapper.toDTO(((UserRepository) repository).findUserByEmail(email));
    }

    public boolean checkPassword(String password, UserDetails foundUser) {
        return bCryptPasswordEncoder.matches(password, foundUser.getPassword());
    }

    public void sendChangePasswordEmail(final UserDTO userDTO) {
        UUID uuid = UUID.randomUUID();
        log.info("Generated Token: {}", uuid);
        userDTO.setChangePasswordToken(uuid.toString());
        update(userDTO);

        SimpleMailMessage mailMessage = MailUtils.createMailMessage(
                userDTO.getEmail(),
                MailConstants.MAIL_SUBJECT_FOR_REMEMBER_PASSWORD,
                MailConstants.MAIL_MESSAGE_FOR_REMEMBER_PASSWORD + uuid);

        javaMailSender.send(mailMessage);
    }

    public void changePassword(String uuid, String password) {
        UserDTO userDTO = mapper.toDTO(((UserRepository) repository).findUserByChangePasswordToken(uuid));
        userDTO.setChangePasswordToken(null);
        userDTO.setPassword(bCryptPasswordEncoder.encode(password));
        update(userDTO);
    }

    public List<String> getUserEmailsWithDelayedRentDate() {
        return ((UserRepository) repository).getDelayedEmails();
    }

    public String checkRole(String username) {
        User user = ((UserRepository) repository).findUserByLogin(username);
        return user.getRole().getTitle();
    }

    @Override
    public UserDTO update(UserDTO object) {
        UserDTO userDTO = getOne(object.getId());
        if (!Objects.isNull(object.getName())) {
            userDTO.setName(object.getName());
        }
        if (!Objects.isNull(object.getSurname())) {
            userDTO.setSurname(object.getSurname());
        }
        if (!Objects.isNull(object.getPatronymic())) {
            userDTO.setPatronymic(object.getPatronymic());
        }
        if (!Objects.isNull(object.getGender())) {
            userDTO.setGender(object.getGender());
        }
        if (!Objects.isNull(object.getBirthDate())) {
            userDTO.setBirthDate(object.getBirthDate());
        }
        if (!Objects.isNull(object.getPhone())) {
            userDTO.setPhone(object.getPhone());
        }
        if (!Objects.isNull(object.getAddress())) {
            userDTO.setAddress(object.getAddress());
        }
        if (!Objects.isNull(object.getEmail())) {
            userDTO.setEmail(object.getEmail());
        }
        return super.update(object);
    }
}
