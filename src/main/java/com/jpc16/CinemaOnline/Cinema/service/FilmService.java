package com.jpc16.CinemaOnline.Cinema.service;

import com.jpc16.CinemaOnline.Cinema.dto.FilmDTO;
import com.jpc16.CinemaOnline.Cinema.mapper.FilmMapper;
import com.jpc16.CinemaOnline.Cinema.model.Film;
import com.jpc16.CinemaOnline.Cinema.repository.FilmParticipantRepository;
import com.jpc16.CinemaOnline.Cinema.repository.FilmRepository;
import com.jpc16.CinemaOnline.Cinema.repository.RatingRepository;
import com.jpc16.CinemaOnline.Cinema.utils.FileHelper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class FilmService extends GenericService<Film, FilmDTO> {

    private final FilmParticipantRepository filmParticipantRepository;
    private final RatingRepository ratingRepository;

    public FilmService(FilmParticipantRepository filmParticipantRepository, FilmMapper filmMapper,
                       FilmRepository filmRepository, RatingRepository ratingRepository) {
        super(filmRepository, filmMapper);
        this.filmParticipantRepository = filmParticipantRepository;
        this.ratingRepository = ratingRepository;
    }

    public FilmDTO create(final FilmDTO filmDTO, MultipartFile videoFile, MultipartFile pictureFile) {
        String videoFileName = FileHelper.createFile(videoFile);
        String pictureFileName = FileHelper.createFile(pictureFile);
        filmDTO.setOnlineCopyPath(videoFileName);
        filmDTO.setPicture(pictureFileName);
        filmDTO.setCreatedWhen(LocalDateTime.now());
        filmDTO.setCreatedBy(SecurityContextHolder.getContext().getAuthentication().getName());
        return mapper.toDTO(repository.save(mapper.toEntity(filmDTO)));
    }

    public Page<FilmDTO> getAllFilms(Pageable pageable) {
        Page<Film> filmPage = repository.findAll(pageable);
        List<FilmDTO> result = mapper.toDTOs(filmPage.getContent());
        return new PageImpl<>(result, pageable, filmPage.getTotalElements());
    }

    public Page<FilmDTO> searchFilm(FilmDTO filmDTO, Pageable pageable) {
        Page<Film> filmPage = ((FilmRepository) repository).searchFilm(
                filmDTO.getTitle(),
                filmDTO.getPremierYear(),
                filmDTO.getCountry(),
                filmDTO.getGenre(),
                filmDTO.getCategory(),
                pageable);

        List<FilmDTO> result = mapper.toDTOs(filmPage.getContent());
        return new PageImpl<>(result, pageable, filmPage.getTotalElements());
    }


    public List<FilmDTO> findByParticipantId(Long id) {
        List<Film> films = ((FilmRepository) repository).findByParticipantId(id);
        return mapper.toDTOs(films);
    }

    public List<FilmDTO> getPopularFilms() {
        List<Film> films = ratingRepository.find4PopularFilms();
        return mapper.toDTOs(films);
    }

}
