CREATE TABLE Films (
   id            integer PRIMARY KEY,
   title         text NOT NULL,
   premier_year  date NOT NULL,
   country       text,
   genre         text
);

CREATE TABLE FilmDirectors (
   film_id     integer,
   director_id integer
);

CREATE TABLE Directors (
  id             integer PRIMARY KEY,
  directors_fio  text NOT NULL,
  position       text NOT NULL
);

CREATE TABLE Orders (
  id         integer PRIMARY KEY,
  user_id    integer,
  film_id    integer,
  rentData   date NOT NULL,
  rentPeriod date NOT NULL,
  purchase   boolean
);

CREATE TABLE Users (
   id          integer PRIMARY KEY,
   login       text NOT NULL,
   password    text NOT NULL,
   firstName   text NOT NULL,
   lastName    text NOT NULL,
   middleName  text NOT NULL,
   birthData   date NOT NULL,
   phone       text NOT NULL,
   address     text,
   email       text NOT NULL,
   createdWhen date,
   role_id     integer NOT NULL
);

CREATE TABLE Role (
   id           integer PRIMARY KEY,
   title        text NOT NULL,
   description  text
);

ALTER TABLE FilmDirectors ADD FOREIGN KEY (film_id) REFERENCES Films (id);

ALTER TABLE FilmDirectors ADD FOREIGN KEY (director_id) REFERENCES Directors (id);

ALTER TABLE Orders ADD FOREIGN KEY (film_id) REFERENCES Films (id);

ALTER TABLE Orders ADD FOREIGN KEY (user_id) REFERENCES Users (id);

ALTER TABLE Users ADD FOREIGN KEY (role_id) REFERENCES Role (id);
