package com.jpc16.CinemaOnline.Cinema.repository;

import com.jpc16.CinemaOnline.Cinema.model.FilmParticipant;
import com.jpc16.CinemaOnline.Cinema.model.lists.FilmRoles;
import com.jpc16.CinemaOnline.Cinema.model.lists.Gender;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface FilmParticipantRepository extends GenericRepository<FilmParticipant> {


    @Query(nativeQuery = true,
            value = """
                    SELECT fp.* FROM FilmParticipant fp WHERE 
                    CAST(fp.gender as char) like '%' || COALESCE(:gender, '%')
                    and fp.surname ilike '%' || COALESCE(:surname, '%')
                    and fp.name ilike '%' || COALESCE(:name, '%')
                    and fp.patronymic ilike '%' || COALESCE(:patronymic, '%')
                    and fp.position ilike '%' || COALESCE(:position, '%')
                    and fp.birthDate ilike '%' || COALESCE(:birthDate, '%')
                    and fp.birthCountry ilike '%' || COALESCE(:birthCountry, '%')
                    """)
    Page<FilmParticipant> findParticipants(@Param("gender") Gender gender,
                                           @Param("surname") String surname,
                                           @Param("name") String name,
                                           @Param("patronymic") String patronymic,
                                           @Param("position") List<FilmRoles> position,
                                           @Param("birthDate") LocalDate birthDate,
                                           @Param("birthCountry") String birthCountry, Pageable pageable);


}
