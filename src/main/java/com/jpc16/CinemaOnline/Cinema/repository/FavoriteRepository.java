package com.jpc16.CinemaOnline.Cinema.repository;

import com.jpc16.CinemaOnline.Cinema.model.Favorite;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface FavoriteRepository extends GenericRepository<Favorite> {


    @Query("SELECT f FROM Favorite f WHERE f.users = :id")
    Page<Favorite> findAllByUser(@Param("id") Long id,
                                 Pageable pageRequest);
}
