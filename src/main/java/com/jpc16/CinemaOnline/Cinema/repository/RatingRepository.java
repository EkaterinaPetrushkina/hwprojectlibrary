package com.jpc16.CinemaOnline.Cinema.repository;

import com.jpc16.CinemaOnline.Cinema.model.Film;
import com.jpc16.CinemaOnline.Cinema.model.Rating;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RatingRepository extends GenericRepository<Rating>{

    @Query("SELECT r FROM Rating r WHERE r.filmId = :id")
    List<Rating> findByFilmId(Long id);

    @Query("SELECT r.filmId, AVG(r.rate) FROM Rating r GROUP BY r.filmId ORDER BY r.rate ASC LIMIT 4")
    List<Film> find4PopularFilms();
}
