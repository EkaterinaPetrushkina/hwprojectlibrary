package com.jpc16.CinemaOnline.Cinema.repository;

import com.jpc16.CinemaOnline.Cinema.model.Film;
import com.jpc16.CinemaOnline.Cinema.model.lists.Category;
import com.jpc16.CinemaOnline.Cinema.model.lists.Genre;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface FilmRepository extends GenericRepository<Film> {

    @Query("SELECT f FROM Film f WHERE f.title = :title OR f.premierYear = :year OR f.country = :country " +
            "OR f.genre = :genre OR f.category = :category")
    Page<Film> searchFilm(@Param(value = "title") String title,
                          @Param(value = "year") LocalDate premierYear,
                          @Param(value = "country") String country,
                          @Param(value = "genre") Genre genre,
                          @Param(value = "category") Category category,
                          Pageable pageable);

    @Query("SELECT f FROM Film f WHERE f.filmParticipants = :id")
    List<Film> findByParticipantId(@Param("id") Long id);

//    @Query("""
//          select case when count(f) > 0 then false else true end
//          from Film f join Order o on f.id = o.film.id
//          where f.id = :id and o.purchase = false
//          """)
//    boolean isBookCanBeDeleted(final Long id);


}
