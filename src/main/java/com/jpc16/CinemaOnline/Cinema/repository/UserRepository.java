package com.jpc16.CinemaOnline.Cinema.repository;

import com.jpc16.CinemaOnline.Cinema.model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends GenericRepository<User> {

    //    @Query(nativeQuery = true, value = "select * from users where login = :login")
    User findUserByLogin(String login);

    User findUserByEmail(String email);

    User findUserByChangePasswordToken(String uuid);

    @Query(nativeQuery = true,
            value = """
                    select distinct email
                                from users u join order o on u.id = o.user_id
                                where o.return_date < now()
                                and o.purchase = false
                                and u.is_deleted = false
                                        
                    """)
    List<String> getDelayedEmails();

}
