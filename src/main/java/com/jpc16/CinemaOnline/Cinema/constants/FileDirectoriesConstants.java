package com.jpc16.CinemaOnline.Cinema.constants;

public interface FileDirectoriesConstants {

    String FILMS_UPLOAD_DIRECTORY = "files/films";
    String PARTICIPANTS_UPLOAD_DIRECTORY = "files/participants";
}
