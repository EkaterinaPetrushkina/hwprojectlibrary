package com.jpc16.CinemaOnline.Cinema.constants;

public interface UserRolesConstants {

    String ADMIN = "ADMIN";
    String USER = "USER";
    String MODERATOR = "MODERATOR";
}
