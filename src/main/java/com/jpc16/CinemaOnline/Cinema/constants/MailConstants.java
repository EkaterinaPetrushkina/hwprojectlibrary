package com.jpc16.CinemaOnline.Cinema.constants;

public interface MailConstants {

    String MAIL_MESSAGE_FOR_REMEMBER_PASSWORD = """
            Привет! Вы получили данное письмо потому, что был отправлен запрос с Вашего аккаунта.\n
            Для восстановления Вашего пароля, пройдите по ссылке: http://localhost:8081/users/change_password?uuid=""";

    String MAIL_SUBJECT_FOR_REMEMBER_PASSWORD = "Восстановление пароля на сайте Online Cinema";

}
