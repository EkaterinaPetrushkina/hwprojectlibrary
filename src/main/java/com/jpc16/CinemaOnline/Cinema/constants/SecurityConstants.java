package com.jpc16.CinemaOnline.Cinema.constants;

import java.util.List;

public interface SecurityConstants {

    List<String> RESOURCES_WHITE_LIST = List.of(
            "/resources/**",
            "/static/**",
            "/static/js/**",
            "/static/css/**",
            "/images/**",
            "/",
            "/swagger-ui/**",
            "/webjars/bootstrap/5.3.0/**",
            "/v3/api-docs/**");
    List<String> FILMS_WHITE_LIST = List.of(
            "/films",
            "/films/search",
            "/films/{id}");
    List<String> FILMS_PERMISSION_LIST = List.of(
            "/films/add",
            "/films/update",
            "/films/delete",
            "/films/download/{id}");
    List<String> USERS_WHITE_LIST = List.of(
            "/login/",
            "/users/registration",
            "/users/remember_password",
            "/users/change_password");
    List<String> USERS_REST_WHITE_LIST = List.of("/users/auth");

}
