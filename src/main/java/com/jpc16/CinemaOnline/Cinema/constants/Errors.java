package com.jpc16.CinemaOnline.Cinema.constants;

public interface Errors {
    class Films {
        public static final String FIlM_DELETE_ERROR = "Фильм не может быть удален";
    }

    class Participants {
        public static final String PARTICIPANT_DELETE_ERROR = "Персона не может быть удална";
    }

    class Users {
        public static final String USER_FORBIDDEN_ERROR = "У вас нет прав просматривать информацию о пользователе";
    }

    class REST {
        public static final String DELETE_ERROR_MESSAGE = "Удаление невозможно";
        public static final String AUTH_ERROR_MESSAGE = "Неавторизованный пользователь";
        public static final String ACCESS_ERROR_MESSAGE = "Отказано в доступе!";
        public static final String NOT_FOUND_ERROR_MESSAGE = "Объект не найден!";
    }
}
