//package com.jpc16.hwlibraryproject.Cinema.config;
//
//import com.jpc16.hwlibraryproject.Cinema.service.UserService;
//import com.jpc16.hwlibraryproject.Cinema.utils.MailUtils;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.mail.SimpleMailMessage;
//import org.springframework.mail.javamail.JavaMailSender;
//import org.springframework.scheduling.annotation.Scheduled;
//import org.springframework.stereotype.Component;
//
//import java.util.List;
//
//@Slf4j
//@Component
//public class MailScheduler {
//
//    private UserService userService;
//    private final JavaMailSender javaMailSender;
//
//    public MailScheduler(UserService userService, JavaMailSender javaMailSender) {
//        this.userService = userService;
//        this.javaMailSender = javaMailSender;
//    }
//
//    @Scheduled(cron = "0 0 6 * * ?")
//    public void sentMailsToDebtors() {
//        log.info("Запуск планировщика по проверки должников....");
//        List<String> emails = userService.getUserEmailsWithDelayedRentDate();
//        if (emails.size() > 0) {
//            SimpleMailMessage simpleMailMessage = MailUtils.createMailMessage(
//                    emails.toArray(new String[0]),
//                    "cinemaOnline@bk.ru",
//                    "Reminder",
//                    "You should return rent film!!!");
//            javaMailSender.send(simpleMailMessage);
//        }
//    }
//
//}
