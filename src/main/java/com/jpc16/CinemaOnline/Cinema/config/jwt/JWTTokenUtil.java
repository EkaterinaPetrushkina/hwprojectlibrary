package com.jpc16.CinemaOnline.Cinema.config.jwt;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.function.Function;

@Component
@Slf4j
public class JWTTokenUtil {

    //7 * 24 * 60 * 60 * 1000 = одна неделя в миллисекундах (время жизни токена)
    public static final long JWT_TOKEN_VALIDITY = 604800000;
    public final String secret = "s1e3c5r4e2t";
    private static final ObjectMapper objectMapper = new ObjectMapper();
    public String generateToken(final UserDetails payload) {
        return Jwts.builder()
                .setSubject(payload.toString())  // -> payload in token
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + JWT_TOKEN_VALIDITY))
                .signWith(SignatureAlgorithm.HS512, secret)
                .compact();
    }

    public Boolean validateToken(final String token, UserDetails userDetails) {
        final String username = getUsernameFromToken(token);
        return (username.equals(userDetails.getUsername()) && !isTokenExpired(token));
    }

    private Boolean isTokenExpired(final String token) {
        final Date expiration = getExpirationDateFromToken(token);
        return expiration.before(new Date());
    }

    private Date getExpirationDateFromToken(String token) {
        return getClaimsFromToken(token, Claims::getExpiration);
    }

    public String getUsernameFromToken(final String token) {
        return getValueFromTokenByKey(token, "username");
    }
    public String getRoleFromToken(final String token) {
        return getValueFromTokenByKey(token, "user_role");
    }
    private String getValueFromTokenByKey(final String token, final String key) {
        String claim = getClaimsFromToken(token,Claims::getSubject);
        JsonNode claimJSON = null;
        try {
            claimJSON = objectMapper.readTree(claim);
        } catch (JsonProcessingException ex) {
            log.error("JWTokenUtil#getUsernameFromToken(): {}", ex.getMessage());
        }

        if (claimJSON != null) {
            return claimJSON.get(key).asText();
        } else {
            return null;
        }
    }

    private <T> T getClaimsFromToken(final String token, Function<Claims, T> claimsResolver) {
        final Claims claims = getAllClaimsFromToken(token);
        return claimsResolver.apply(claims);
    }

    private Claims getAllClaimsFromToken(String token) {
        return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
    }

}
