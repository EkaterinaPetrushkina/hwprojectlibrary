//package com.jpc16.hwlibraryproject.Cinema.config;
//
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.mail.javamail.JavaMailSender;
//import org.springframework.mail.javamail.JavaMailSenderImpl;
//
//import java.util.Properties;
//
//@Configuration
//public class EmailConfig {
//
//    @Value("${spring.mail.host}")
//    private String emailServerHost;
//
//    @Value("${spring.mail.port}")
//    private Integer emailServerPort;
//
//    @Value("${spring.mail.username}")
//    private String emailServerUserName;
//
//    @Value("${spring.mail.password}")
//    private String emailServerPassword;
//
//    @Bean
//    public JavaMailSender getJavaMailSender() {
//        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
//        mailSender.setHost(emailServerHost);
//        mailSender.setPort(emailServerPort);
//        mailSender.setUsername(emailServerUserName);
//        mailSender.setPassword(emailServerPassword);
//
//        Properties properties = mailSender.getJavaMailProperties();
//        properties.put("mail.smtp.auth", "true");
//        properties.put("mail.smtp.starttls.enable", "false");
//        properties.put("mail.smtp.ssl.protocols", "TLSv1.2");
//        properties.put("mail.smtp.ssl.enable", "true");
//        properties.put("mail.smtp.ssl.trust", "smtp.mail.ru");
//
//        return mailSender;
//    }
//}
