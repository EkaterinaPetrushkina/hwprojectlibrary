//package com.jpc16.hwlibraryproject.Cinema.config.jwt;
//
//import com.jpc16.hwlibraryproject.Cinema.constants.SecurityConstants;
//import com.jpc16.hwlibraryproject.Cinema.service.userdetails.CustomUserDetailService;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.security.authentication.AuthenticationManager;
//import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
//import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
//import org.springframework.security.config.http.SessionCreationPolicy;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
//import org.springframework.security.web.SecurityFilterChain;
//import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
//
//import static com.jpc16.hwlibraryproject.Cinema.constants.SecurityConstants.*;
//import static com.jpc16.hwlibraryproject.Cinema.constants.UserRolesConstants.*;
//
//@Configuration
//@EnableWebSecurity
//@EnableMethodSecurity
//public class JWTSecurityConfig {
//
//    private final BCryptPasswordEncoder bCryptPasswordEncoder;
//    private final CustomUserDetailService customUserDetailService;
//    private final JWTTokenFilter jwtTokenFilter;
//
//    public JWTSecurityConfig(BCryptPasswordEncoder bCryptPasswordEncoder,
//                             CustomUserDetailService customUserDetailService, JWTTokenFilter jwtTokenFilter) {
//        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
//        this.customUserDetailService = customUserDetailService;
//        this.jwtTokenFilter = jwtTokenFilter;
//    }
//
//    @Bean
//    public SecurityFilterChain securityFilterChain(HttpSecurity httpSecurity) throws Exception {
//        httpSecurity
//                .cors().disable()
//                .csrf().disable()
//                .authorizeHttpRequests((requests) -> requests
//                        .requestMatchers(RESOURCES_WHITE_LIST.toArray(String[]::new)).permitAll()
//                        .requestMatchers(USERS_REST_WHITE_LIST.toArray(String[]::new)).permitAll()
//                     //   .requestMatchers("/directors/**").hasAnyRole(ADMIN, USER)
//                     //   .anyRequest().authenticated()
//                        .requestMatchers(FILMS_WHITE_LIST.toArray(String[]::new)).permitAll()
//                        .requestMatchers(USERS_WHITE_LIST.toArray(String[]::new)).permitAll()
//                        .requestMatchers(FILMS_PERMISSION_LIST.toArray(String[]::new)).hasAnyRole(ADMIN, MODERATOR)
//                        .anyRequest().authenticated()
//                )
//                .exceptionHandling()
//               // .authenticationEntryPoint()
//                .and()
//                .sessionManagement(session -> session.sessionCreationPolicy(SessionCreationPolicy.STATELESS))
//                .addFilterBefore(jwtTokenFilter, UsernamePasswordAuthenticationFilter.class)
//                .userDetailsService(customUserDetailService);
//        return httpSecurity.build();
//    }
//
//    @Bean
//    public AuthenticationManager authenticationManager(AuthenticationConfiguration authenticationConfiguration) throws Exception {
//        return authenticationConfiguration.getAuthenticationManager();
//    }
//}
