package com.jpc16.CinemaOnline.Cinema.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jpc16.CinemaOnline.Cinema.model.lists.FilmRoles;
import com.jpc16.CinemaOnline.Cinema.model.lists.Gender;
import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name = "Directors")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SequenceGenerator(name = "default_generator", sequenceName = "directors_sequence", allocationSize = 1)
public class FilmParticipant extends GenericModel {

    @Column(name = "gender")
    @Enumerated
    private Gender gender;

    @Column(name = "surname", nullable = false)
    private String surname;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "patronymic")
    private String patronymic;

    @Column(name = "position", nullable = false)
    @Enumerated
    private List<FilmRoles> position;

    @Column(name = "birth_date", nullable = false)
    private LocalDate birthDate;

    @Column(name = "birth_country", nullable = false)
    private String birthCountry;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JsonIgnore
    @JoinTable(name = "Film_Participant", joinColumns = @JoinColumn(name = "participant_id"),
            foreignKey = @ForeignKey(name = "FK_PARTICIPANT_FILMS"),
            inverseJoinColumns = @JoinColumn(name = "film_id"),
            inverseForeignKey = @ForeignKey(name = "FK_FILMS_PARTICIPANT"))
    List<Film> films;
}
