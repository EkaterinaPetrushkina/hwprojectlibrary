package com.jpc16.CinemaOnline.Cinema.model.lists;

public enum Genre {
    ACTION("Боевик"),
    COMEDY("Комедия"),
    DETECTIVE("Детектив"),
    DRAMA("Драма"),
    FANTASTIC("Фантастика"),
    HORROR("Ужастик"),
    MUSICAL("Мюзикл"),
    ROMANTIC("Романтика"),
    SCIENCE_FICTION("Sci-Fi");

    private final String genreTextDisplay;

    Genre(String text) {
        this.genreTextDisplay = text;
    }


    public String getGenreTextDisplay() {
        return this.genreTextDisplay;
    }
}
