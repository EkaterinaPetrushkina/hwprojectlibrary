package com.jpc16.CinemaOnline.Cinema.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jpc16.CinemaOnline.Cinema.model.lists.Category;
import com.jpc16.CinemaOnline.Cinema.model.lists.Genre;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name = "Films")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SequenceGenerator(name = "default_generator", sequenceName = "films_sequence", allocationSize = 1)
public class Film extends GenericModel {

    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "premier_year", nullable = false)
    private LocalDate premierYear;

    @Column(name = "country")
    private String country;

    @Column(name = "genre")
    @Enumerated(value = EnumType.STRING)
    private Genre genre;

    @Column(name = "category")
    @Enumerated
    private Category category;

    @Column(name = "duration")
    private String duration;

    @Column(name = "picture")
    private String picture;

    @Column(name = "online_copy_path", nullable = false)
    private String onlineCopyPath;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JsonIgnore
    @JoinTable(name = "Film_Participant", joinColumns = @JoinColumn(name = "film_id"),
            foreignKey = @ForeignKey(name = "FK_FILMS_PARTICIPANT"),
            inverseJoinColumns = @JoinColumn(name = "participant_id"),
            inverseForeignKey = @ForeignKey(name = "FK_PARTICIPANT_FILMS"))
    private List<FilmParticipant> filmParticipants;

    @ManyToMany
    @JoinTable(name = "Favorite_Film", joinColumns = @JoinColumn(name = "film_id"),
            foreignKey = @ForeignKey(name = "FK_FILM_FAVORITE"),
            inverseJoinColumns = @JoinColumn(name = "favorite_id"),
            inverseForeignKey = @ForeignKey(name = "FK_FAVORITE_FILM"))
    private List<Favorite> favorites;

    @OneToMany(mappedBy = "filmId")
    private List<Rating> ratings;

}
