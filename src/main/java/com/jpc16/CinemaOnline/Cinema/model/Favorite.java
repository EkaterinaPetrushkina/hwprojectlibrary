package com.jpc16.CinemaOnline.Cinema.model;

import com.jpc16.CinemaOnline.Cinema.model.lists.Status;
import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@Entity
@Table(name = "Favorite")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SequenceGenerator(name = "default_generator", sequenceName = "order_sequence", allocationSize = 1)
public class Favorite extends GenericModel {

    @Column(name = "status")
    @Enumerated
    private Status status;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "Favorite_User", joinColumns = @JoinColumn(name = "favorite_id"),
            foreignKey = @ForeignKey(name = "FK_FAVORITE_USER"),
            inverseJoinColumns = @JoinColumn(name = "user_id"),
            inverseForeignKey = @ForeignKey(name = "FK_USER_FAVORITE"))
    private List<User> users;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "Favorite_Film", joinColumns = @JoinColumn(name = "favorite_id"),
            foreignKey = @ForeignKey(name = "FK_FAVORITE_FILM"),
            inverseJoinColumns = @JoinColumn(name = "film_id"),
            inverseForeignKey = @ForeignKey(name = "FK_FILM_FAVORITE"))
    private List<Film> films;



}
