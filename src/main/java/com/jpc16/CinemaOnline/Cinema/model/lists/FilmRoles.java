package com.jpc16.CinemaOnline.Cinema.model.lists;

public enum FilmRoles {

    ACTOR("Актер"),
    DIRECTOR("Режиссер"),
    SCREENWRITER("Сценарист"),
    MODEL("Модель"),
    OTHER("Другое");

    private final String roleTextDisplay;
    FilmRoles(String text) {
        this.roleTextDisplay = text;
    }

    public String getRoleTextDisplay() {
        return this.roleTextDisplay;
    }
}
