package com.jpc16.CinemaOnline.Cinema.model;

import com.jpc16.CinemaOnline.Cinema.model.lists.Gender;
import jakarta.persistence.*;
import lombok.*;

import java.util.Date;
import java.util.List;

@Entity
@Table(name = "Users",
       uniqueConstraints = {@UniqueConstraint(name = "uniquePhone", columnNames = "phone"),
       @UniqueConstraint(name = "uniqueEmail", columnNames = "email"),
       @UniqueConstraint(name = "uniqueLogin", columnNames = "login")})
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SequenceGenerator(name = "default_generator", sequenceName = "users_sequence", allocationSize = 1)
public class User extends GenericModel {

    @Column(name = "login", nullable = false)
    private String login;

    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "gender")
    private Gender gender;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "surname", nullable = false)
    private String surname;

    @Column(name = "patronymic")
    private String patronymic;

    @Column(name = "birth_date")
    private Date birthDate;

    @Column(name = "phone", nullable = false)
    private String phone;

    @Column(name = "address")
    private String address;

    @Column(name = "email", nullable = false)
    private String email;

    @Column(name = "change_password_token")
    private String changePasswordToken;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "role_id", nullable = false,
                foreignKey = @ForeignKey(name = "FK_USERS_ROLES"))
    private Role role;

    @ManyToMany
    @JoinTable(name = "Favorite_User", joinColumns = @JoinColumn(name = "user_id"),
            foreignKey = @ForeignKey(name = "FK_USER_FAVORITE"),
            inverseJoinColumns = @JoinColumn(name = "favorite_id"),
            inverseForeignKey = @ForeignKey(name = "FK_FAVORITE_USER"))
    private List<Favorite> favorites;

    @OneToMany(mappedBy = "userId")
    private List<Rating> ratings;
}
