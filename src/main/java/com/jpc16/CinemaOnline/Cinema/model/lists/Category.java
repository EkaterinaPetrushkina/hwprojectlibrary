package com.jpc16.CinemaOnline.Cinema.model.lists;

public enum Category {

    MOVIE("Фильм"),
    SERIAL("Сериал");

    private final String categoryTextDisplay;

    Category(String text) {
        this.categoryTextDisplay = text;
    }

    public String getCategoryTextDisplay() {
        return this.categoryTextDisplay;
    }
}
