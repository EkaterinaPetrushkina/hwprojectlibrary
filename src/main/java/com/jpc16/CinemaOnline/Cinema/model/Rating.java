package com.jpc16.CinemaOnline.Cinema.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Entity
@Table(name = "Rating")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@SequenceGenerator(name = "default_generator", sequenceName = "rating_generator", allocationSize = 1)
public class Rating extends GenericModel {

    @Column(name = "rate")
    private Integer rate;

    @ManyToOne
    @JoinColumn(name = "film_id",
            foreignKey = @ForeignKey(name = "FK_FILM_FILM"))
    private Film filmId;

    @ManyToOne
    @JoinColumn(name = "user_id",
            foreignKey = @ForeignKey(name = "FK_FILM_USER"))
    private User userId;
}
