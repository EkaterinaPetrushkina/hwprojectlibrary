package com.jpc16.CinemaOnline.Cinema.model.lists;

public enum Status {

    VIEWED("Просмотрено"),
    WANT_TO_SEE("Хочу посмотреть"),
    IN_PROCESS("Смотрю"),
    FAVORITE("Любимые");

    private final String statusTextDisplay;

    Status(String text) {
        this.statusTextDisplay = text;
    }

    public String getStatusTextDisplay() {
        return this.statusTextDisplay;
    }
}
