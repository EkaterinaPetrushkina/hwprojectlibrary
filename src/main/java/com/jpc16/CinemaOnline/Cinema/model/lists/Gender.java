package com.jpc16.CinemaOnline.Cinema.model.lists;

public enum Gender {

    FEMALE("Женщина"),
    MALE("Мужчина");

    private final String genderTextDisplay;

    Gender(String text) {
        this.genderTextDisplay = text;
    }

    public String getGenderTextDisplay() {
        return this.genderTextDisplay;
    }

}
