package com.jpc16.CinemaOnline.Cinema.exception;

public class MyDeleteException extends Exception {
    public MyDeleteException(final String message) {
        super(message);
    }
}
