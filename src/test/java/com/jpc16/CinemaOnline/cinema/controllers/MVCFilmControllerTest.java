//package com.jpc16.hwlibraryproject.cinema.controllers;
//
//import com.jpc16.hwlibraryproject.Cinema.dto.FilmDTO;
//import com.jpc16.hwlibraryproject.Cinema.model.lists.Genre;
//import com.jpc16.hwlibraryproject.Cinema.service.FilmService;
//import jakarta.transaction.Transactional;
//import lombok.extern.slf4j.Slf4j;
//import org.junit.jupiter.api.DisplayName;
//import org.junit.jupiter.api.Order;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.mockito.junit.jupiter.MockitoExtension;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.MediaType;
//import org.springframework.security.test.context.support.WithAnonymousUser;
//import org.springframework.security.test.context.support.WithMockUser;
//import org.springframework.test.annotation.Rollback;
//import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
//
//import java.time.LocalDate;
//import java.util.ArrayList;
//
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
//
//@Slf4j
//@Transactional
//@Rollback(value = false)
//@ExtendWith(MockitoExtension.class)
//public class MVCFilmControllerTest extends CommonTestMVC {
//
//    @Autowired
//    private FilmService filmService;
//
//    private final FilmDTO filmDTO = new FilmDTO(
//            "MVC_TestFilmTitle",
//            LocalDate.of(1900,01,01),
//            "Test Country",
//            Genre.ACTION,
//            "Test Duration",
//            10,
//            "Test Online Copy",
//            new ArrayList<>(),
//            new ArrayList<>());
//
//    @Override
//    @Test
//    @DisplayName("View all films through MVC Controller")
//    @Order(1)
//    @WithAnonymousUser
//    protected void listAll() throws Exception {
//        log.info("Testing listAll()");
//        mvc.perform(MockMvcRequestBuilders.get("/films")
//                        .accept(MediaType.APPLICATION_JSON)
//                        .contentType(MediaType.APPLICATION_JSON))
//                .andExpect(status().is2xxSuccessful())
//                .andExpect(view().name("films/viewAllFilms"))
//                .andExpect(model().attributeExists("films"))
//                .andReturn();
//        log.info("The end of the test listAll()");
//
//    }
//
//    @Override
//    @Test
//    @Order(2)
//    @DisplayName("Create film through MVC Controller")
//    @WithMockUser(username = "admin", roles = "ADMIN", password = "admin")
//    protected void createObject() throws Exception {
//        log.info("Testing createObject()");
//        mvc.perform(MockMvcRequestBuilders.post("/films/add")
//                        .contentType(MediaType.APPLICATION_JSON)
//                        .accept(MediaType.APPLICATION_JSON)
//                        .flashAttr("filmForm", filmDTO))
//                .andExpect(status().is3xxRedirection())
//                .andExpect(view().name("redirect:/films"))
//                .andExpect(redirectedUrlTemplate("/films"))
//                .andExpect(redirectedUrl("/films"))
//                .andReturn();
//        log.info("The end of the test createObject()");
//
//    }
//
//
//    @Override
//    @Test
//    @Order(2)
//    @DisplayName("Update film through MVC Controller")
//    @WithMockUser(username = "admin", roles = "ADMIN", password = "admin")
//    protected void updateObject() throws Exception {
//        log.info("Testing updateObject()");
//        FilmDTO film = filmService.findFilmByTitle(filmDTO.getTitle());
//        film.setTitle(filmDTO.getTitle());
//        mvc.perform(MockMvcRequestBuilders.post("/films/update")
//                        .contentType(MediaType.APPLICATION_JSON)
//                        .flashAttr("filmForm", film)
//                        .accept(MediaType.APPLICATION_JSON)
//                ).andExpect(status().is3xxRedirection())
//                .andExpect(view().name("redirect:/films"))
//                .andExpect(redirectedUrlTemplate("/films"))
//                .andExpect(redirectedUrl("/films"))
//                .andReturn();
//        log.info("The end of the test updateObject()");
//    }
//
//    @Test
//    @DisplayName("Delete film through MVC Controller")
//    @Order(3)
//    @WithMockUser(username = "admin", roles = "ADMIN", password = "admin")
//    protected void deleteObject() throws Exception {
//        log.info("Testing deleteObject()");
//        FilmDTO film = filmService.findFilmByTitle(filmDTO.getTitle());
//        mvc.perform(MockMvcRequestBuilders.delete("/films/delete/{id}", film.getId())
//                        .contentType(MediaType.APPLICATION_JSON)
//                        .accept(MediaType.APPLICATION_JSON)
//                ).andExpect(status().is2xxSuccessful())
//                .andReturn();
//        log.info("The end of the test deleteObject()");
//    }
//
//}
