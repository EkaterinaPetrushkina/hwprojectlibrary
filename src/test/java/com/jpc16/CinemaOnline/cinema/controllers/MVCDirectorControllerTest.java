//package com.jpc16.hwlibraryproject.cinema.controllers;
//
//
//import com.jpc16.hwlibraryproject.Cinema.dto.FilmParticipantDTO;
//import com.jpc16.hwlibraryproject.Cinema.service.FilmParticipantService;
//import org.junit.jupiter.api.extension.ExtendWith;
//import jakarta.transaction.Transactional;
//import lombok.extern.slf4j.Slf4j;
//import org.junit.jupiter.api.DisplayName;
//import org.junit.jupiter.api.Order;
//import org.junit.jupiter.api.Test;
//import org.mockito.junit.jupiter.MockitoExtension;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.domain.PageRequest;
//import org.springframework.data.domain.Sort;
//import org.springframework.http.MediaType;
//import org.springframework.security.test.context.support.WithAnonymousUser;
//import org.springframework.security.test.context.support.WithMockUser;
//import org.springframework.test.annotation.Rollback;
//import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
//
//import java.time.LocalDate;
//import java.util.ArrayList;
//
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
//import static org.junit.jupiter.api.Assertions.assertTrue;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
//import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
//
//
//
//@Slf4j
//@Transactional
//@Rollback(value = false)
//@ExtendWith(MockitoExtension.class)
//public class MVCDirectorControllerTest extends CommonTestMVC {
//
//    @Autowired
//    private FilmParticipantService filmParticipantService;
//
//    private final FilmParticipantDTO filmParticipantDTO = new FilmParticipantDTO(
//            "directorFIO",
//            "position",
//            LocalDate.of(1990, 01,01),
//            "country",
//            new ArrayList<>());
//
//    private final FilmParticipantDTO filmParticipantDTOUpdated = new FilmParticipantDTO(
//            "directorFIO",
//            "position",
//            LocalDate.of(1900, 01, 01),
//            "country",
//            new ArrayList<>());
//
//    @Override
//    @Test
//    @DisplayName("View all directors through MVC Controller")
//    @Order(1)
//    @WithAnonymousUser
//    protected void listAll() throws Exception {
//        log.info("Testing listAll()");
//        mvc.perform(MockMvcRequestBuilders.get("/directors")
//                        .accept(MediaType.APPLICATION_JSON)
//                        .contentType(MediaType.APPLICATION_JSON))
//                .andExpect(status().is2xxSuccessful())
//                .andExpect(view().name("directors/viewAllDirectors"))
//                .andExpect(model().attributeExists("directors"))
//                .andReturn();
//        log.info("The end of the test listAll()");
//
//    }
//
//    @Override
//    @Test
//    @Order(1)
//    @DisplayName("Create director through MVC Controller")
//    @WithMockUser(username = "admin", roles = "ADMIN", password = "admin")
//    protected void createObject() throws Exception {
//        log.info("Testing createObject()");
//        mvc.perform(MockMvcRequestBuilders.post("/directors/add")
//                        .contentType(MediaType.APPLICATION_JSON)
//                        .accept(MediaType.APPLICATION_JSON)
//                        .flashAttr("directorForm", filmParticipantDTO))
//                .andExpect(status().is3xxRedirection())
//                .andExpect(view().name("redirect:/directors"))
//                .andExpect(redirectedUrlTemplate("/directors"))
//                .andExpect(redirectedUrl("/directors"))
//                .andReturn();
//        log.info("The end of the test createObject()");
//
//    }
//
//    @Override
//    @Test
//    @Order(2)
//    @DisplayName("Update director through MVC Controller")
//    @WithMockUser(username = "admin", roles = "ADMIN", password = "admin")
//    protected void updateObject() throws Exception {
//        log.info("Testing updateObject()");
//        PageRequest pageRequest = PageRequest.of(0, 5, Sort.by(Sort.Direction.ASC, "directorFIO"));
//        FilmParticipantDTO director = filmParticipantService.findDirectorByDirectorsFIO(filmParticipantDTO.getName());
//        director.setName(filmParticipantDTOUpdated.getName());
//        mvc.perform(post("/directors/update")
//                        .contentType(MediaType.APPLICATION_JSON_VALUE)
//                        .accept(MediaType.APPLICATION_JSON)
//                        .flashAttr("directorForm", director)
//                )
//                .andDo(print())
//                .andExpect(status().is3xxRedirection())
//                .andExpect(view().name("redirect:/directors"))
//                .andExpect(redirectedUrlTemplate("/directors"))
//                .andExpect(redirectedUrl("/directors"))
//                .andReturn();;
//        log.info("The end of the test updateObject()");
//    }
//
//    @Test
//    @DisplayName("Delete director through MVC Controller")
//    @Order(3)
//    @WithMockUser(username = "admin", roles = "ADMIN", password = "admin")
//    protected void deleteObject() throws Exception {
//        log.info("Testing deleteObject()");
//        PageRequest pageRequest = PageRequest.of(0, 5, Sort.by(Sort.Direction.ASC, "directorsFIO"));
//        FilmParticipantDTO foundDirectorForDelete = filmParticipantService
//                .searchParticipant(filmParticipantDTOUpdated.getDirectorsFIO(), pageRequest).getContent().get(0);
//        foundDirectorForDelete.setDeleted(true);
//        mvc.perform(get("/directors/delete/{id}", foundDirectorForDelete.getId())
//                        .contentType(MediaType.APPLICATION_JSON)
//                        .accept(MediaType.APPLICATION_JSON)
//                )
//                .andDo(print())
//                .andExpect(status().is3xxRedirection())
//                .andExpect(view().name("redirect:/directors"))
//                .andExpect(redirectedUrl("/directors"));
//
//        FilmParticipantDTO deletedDirector = filmParticipantService.getOne(foundDirectorForDelete.getId());
//        assertTrue(deletedDirector.isDeleted());
//        log.info("The end of the test softDeleteObject()");
//    }
//
//
//
//}
