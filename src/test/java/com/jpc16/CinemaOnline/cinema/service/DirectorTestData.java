//package com.jpc16.hwlibraryproject.cinema.service;
//
//import com.jpc16.hwlibraryproject.Cinema.dto.FilmParticipantDTO;
//
//import java.time.LocalDate;
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.List;
//
//public interface DirectorTestData {
//    FilmParticipantDTO DIRECTOR_DTO_1 = new FilmParticipantDTO(
//            "directorFIO1",
//            "position1",
//            LocalDate.of(1900, 01, 01),
//            "country",
//            new ArrayList<>());
//
//    FilmParticipantDTO DIRECTOR_DTO_2 = new FilmParticipantDTO(
//            "directorFIO2",
//            "position2",
//            LocalDate.of(1900, 01, 01),
//            "country",
//            new ArrayList<>());
//
//
//    FilmParticipantDTO DIRECTOR_DTO_3_DELETED = new FilmParticipantDTO(
//            "directorFIO3",
//            "position3",
//            LocalDate.of(1900, 01, 01),
//            "country",
//            new ArrayList<>());
//
//
//    List<FilmParticipantDTO> DIRECTOR_DTO_LIST = Arrays.asList(DIRECTOR_DTO_1, DIRECTOR_DTO_2, DIRECTOR_DTO_3_DELETED);
//
//
//    Director DIRECTOR_1 = new Director(
//            "director1",
//            "position1",
//            LocalDate.of(1900, 01, 01),
//            "country",
//
//            null);
//
//    Director DIRECTOR_2 = new Director(
//            "director2",
//            "position2",
//            LocalDate.of(1900, 01, 01),
//            "country",
//            null);
//
//    Director DIRECTOR_3 = new Director(
//            "director3",
//            "position3",
//            LocalDate.of(1900, 01, 01),
//            "country",
//            null);
//
//    List<Director> DIRECTOR_LIST = Arrays.asList(DIRECTOR_1, DIRECTOR_2, DIRECTOR_3);
//}
//
