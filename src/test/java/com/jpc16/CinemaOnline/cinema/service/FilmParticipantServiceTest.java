//package com.jpc16.hwlibraryproject.cinema.service;
//
//import com.jpc16.hwlibraryproject.Cinema.dto.AddDirectorDTO;
//import com.jpc16.hwlibraryproject.Cinema.dto.FilmParticipantDTO;
//import com.jpc16.hwlibraryproject.Cinema.mapper.DirectorMapper;
//import com.jpc16.hwlibraryproject.Cinema.mapper.FilmParticipantMapper;
//import com.jpc16.hwlibraryproject.Cinema.model.Director;
//import com.jpc16.hwlibraryproject.Cinema.model.FilmParticipant;
//import com.jpc16.hwlibraryproject.Cinema.repository.FilmParticipantRepository;
//import com.jpc16.hwlibraryproject.Cinema.service.FilmParticipantService;
//import lombok.extern.slf4j.Slf4j;
//import org.junit.jupiter.api.MethodOrderer;
//import org.junit.jupiter.api.Order;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.TestMethodOrder;
//import org.mockito.Mockito;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.junit.jupiter.api.Assertions.assertTrue;
//
//import java.util.List;
//import java.util.Optional;
//
//@Slf4j
//@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
//public class FilmParticipantServiceTest extends  GenericTest<FilmParticipant, FilmParticipantDTO> {
//
//
//    public FilmParticipantServiceTest() {
//        super();
//        repository = Mockito.mock(FilmParticipantRepository.class);
//        mapper = Mockito.mock(FilmParticipantMapper.class);
//        service = new FilmParticipantService((FilmParticipantRepository) repository, (FilmParticipantMapper) mapper);
//    }
//
//    @Test
//    @Order(1)
//    @Override
//    protected void getAll() {
//        Mockito.when(repository.findAll()).thenReturn(DirectorTestData.DIRECTOR_LIST);
//        Mockito.when(mapper.toDTOs(DirectorTestData.DIRECTOR_LIST)).thenReturn(DirectorTestData.DIRECTOR_DTO_LIST);
//
//        List<FilmParticipantDTO> filmParticipantDTOS = service.listAll();
//        log.info("Testing getALL: {}", filmParticipantDTOS);
//        assertEquals(DirectorTestData.DIRECTOR_LIST.size(), filmParticipantDTOS.size());
//    }
//
//    @Test
//    @Order(2)
//    @Override
//    protected void getOne() {
//        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(DirectorTestData.DIRECTOR_1));
//        Mockito.when(mapper.toDTO(DirectorTestData.DIRECTOR_1)).thenReturn(DirectorTestData.DIRECTOR_DTO_1);
//
//        FilmParticipantDTO filmParticipantDTO = service.getOne(1L);
//        log.info("Testing getOne: {}", filmParticipantDTO);
//        assertEquals(DirectorTestData.DIRECTOR_DTO_1, filmParticipantDTO);
//    }
//
//    @Test
//    @Order(3)
//    @Override
//    protected void create() {
//        Mockito.when(mapper.toEntity(DirectorTestData.DIRECTOR_DTO_1)).thenReturn(DirectorTestData.DIRECTOR_1);
//        Mockito.when(mapper.toDTO(DirectorTestData.DIRECTOR_1)).thenReturn(DirectorTestData.DIRECTOR_DTO_1);
//        Mockito.when(repository.save(DirectorTestData.DIRECTOR_1)).thenReturn(DirectorTestData.DIRECTOR_1);
//
//        FilmParticipantDTO filmParticipantDTO = service.create(DirectorTestData.DIRECTOR_DTO_1);
//        log.info("Testing creat: {}", filmParticipantDTO);
//    }
//
//    @Test
//    @Order(4)
//    @Override
//    protected void update() {
//        Mockito.when(mapper.toEntity(DirectorTestData.DIRECTOR_DTO_1)).thenReturn(DirectorTestData.DIRECTOR_1);
//        Mockito.when(mapper.toDTO(DirectorTestData.DIRECTOR_1)).thenReturn(DirectorTestData.DIRECTOR_DTO_1);
//        Mockito.when(repository.save(DirectorTestData.DIRECTOR_1)).thenReturn(DirectorTestData.DIRECTOR_1);
//
//        FilmParticipantDTO filmParticipantDTO = service.update(DirectorTestData.DIRECTOR_DTO_1);
//        log.info("Testing update: {}", filmParticipantDTO);
//    }
//
//    @Test
//    @Order(5)
//    @Override
//    protected void add() {
//        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(DirectorTestData.DIRECTOR_1));
//        Mockito.when(service.getOne(1L)).thenReturn(DirectorTestData.DIRECTOR_DTO_1);
//        Mockito.when(repository.save(DirectorTestData.DIRECTOR_1)).thenReturn(DirectorTestData.DIRECTOR_1);
//
//        ((FilmParticipantService) service).addFilm(new AddDirectorDTO(1L, 1L));
//        log.info("Testing addFilm: {}", DirectorTestData.DIRECTOR_DTO_1.getFilmsId());
//        assertTrue(DirectorTestData.DIRECTOR_DTO_1.getFilmsId().size() >= 1);
//    }
//
//
//}
